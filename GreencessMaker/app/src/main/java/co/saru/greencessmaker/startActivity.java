package co.saru.greencessmaker;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by saru on 2016. 8. 5..
 */
public class startActivity extends Activity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.startpage);
        Handler hd = new Handler();
        hd.postDelayed(new splashhandler() , 1000); // 1초 후에 hd Handler 실행
    }

    private class splashhandler implements Runnable{
        public void run() {
            startActivity(new Intent(getApplication(), testActivity1.class)); // 로딩이 끝난후 이동할 Activity
            startActivity.this.finish(); // 로딩페이지 Activity Stack에서 제거
        }
    }

}
