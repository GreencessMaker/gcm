package co.saru.greencessmaker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by saru on 2016. 8. 16..
 */
public class imageViewAdapter extends BaseAdapter {
    String flag;
    private ArrayList<ListViewItem> imageList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> dateList = new ArrayList<ListViewItem>() ;



    // ListViewAdapter의 생성자
    public imageViewAdapter() {

    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return imageList.size() ;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_image, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ViewHolder holder = new ViewHolder();
        holder.imageView = null;

        TextView dateTextView = (TextView) convertView.findViewById(R.id.date) ;
        holder.imageView = (ImageView)convertView.findViewById(R.id.potitem_image);


        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        ListViewItem listViewItem1 = dateList.get(position);
        ListViewItem listViewItem2 = imageList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        dateTextView.setText(listViewItem1.getTitle());

        if(listViewItem1.getTitle().equals("null")) {
            Drawable drawable = convertView.getResources().getDrawable(R.drawable.default_image);
            holder.imageView.setImageDrawable(drawable);

        }

        else
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            //이미지 설정.
            try {
                String imageURL = "http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/images/1"+"-"+listViewItem2.getTitle()+".png";
                Log.d("@@@@@@@@",imageURL);
                URL url = new URL(imageURL);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                Bitmap bm = BitmapFactory.decodeStream(bis);
                bis.close();
                holder.imageView.setImageBitmap(bm);
            } catch (Exception e) {
            }

        }

        convertView.setTag(holder);
        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return imageList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem(String image,String date) {
        ListViewItem item1 = new ListViewItem();
        ListViewItem item2 = new ListViewItem();

        item1.setTitle(image);
        imageList.add(item1);

        item2.setTitle(date);
        dateList.add(item2);

    }

    // 뷰홀더 생성
    private static class ViewHolder{
        ImageView imageView;

    }
}

