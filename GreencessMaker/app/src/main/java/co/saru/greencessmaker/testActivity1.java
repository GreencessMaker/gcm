package co.saru.greencessmaker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saru on 2016. 11. 7..
 */
public class testActivity1 extends Activity{
    String[] connectlist ={null};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectpot);


/*
        final String URL="http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getDevicesByUserId/kwonssy02";
        String jsonMessage = getJsonMessage(URL);
        JSONArray jAr = null;
        try {
            jAr = new JSONArray(jsonMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] list = new String[jAr.length()] ;
        for(int i=0; i< list.length ;i++)
        {
            list[i] = output(jsonMessage, "deviceName",i);
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list);
        final ListView recordListView = (ListView)findViewById(R.id.pot_ListView);
        recordListView.setAdapter(adapter);

*/
        final ListView potListView = (ListView)findViewById(R.id.pot_ListView);
        showPotList(potListView);




        //눌렀을때 다음행동.
        potListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //기기 연결상관없이 하고 싶으면 여기수정


                if(connectlist[position].equals("1")) {

                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    intent.putExtra("type", String.valueOf(position + 1));
                    startActivity(intent);
                    finish();
                    //startActivity(new Intent(getApplication(), MainActivity.class));
                    //Toast.makeText(getBaseContext(), list[+position],Toast.LENGTH_SHORT).show();
                }

                else if(connectlist[position].equals("0")){
                    Toast.makeText(getApplicationContext(), "화분연결을 확인해주세요.", Toast.LENGTH_SHORT).show();
                }



                /*
                Intent intent = new Intent(getApplication(), MainActivity.class);
                intent.putExtra("type", String.valueOf(position + 1));
                startActivity(intent);
                finish();
                */


            }
        });


    }

    public String getJsonMessage(String URL) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //String kidsyamUrl = "https://test-api.kidsyam.com:3443/testapi/test";

        String send="plz";

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(URL);

            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();

            if (resEntityGet != null) {
                //do something with the response
                // Log.d("GET RESPONSE", EntityUtils.toString(resEntityGet));
                send = EntityUtils.toString(resEntityGet);
                //String send = URLEncoder.encode("한글","utf-8");  // 원래는 utf-8 처리를 하는게 맞음.
                return send;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return send;
    }

    public String output(String input, String fistValue ,int i){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            //  JSONArray jAr = new JSONArray("["+input+"]");
            JSONArray jAr = new JSONArray(input);
            // 개별 객체를 하나씩 추출
            JSONObject obj1 = jAr.getJSONObject(i);
            // 객체에서 데이터를 추출
            strData += obj1.getString(fistValue);

        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

    public void showPotList(ListView lv){
        ListView listview ;
        final potAdapter adapter;


        // Adapter 생성
        adapter = new potAdapter() ;
        // 리스트뷰 참조 및 Adapter달기
        listview = lv;
        listview.setAdapter(adapter);

        final String potURL="http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getDevicesByUserId/kwonssy02";
        JSONArray potjAr = null;
        try {
            potjAr = new JSONArray(getJsonMessage(potURL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] namelist = new String[potjAr.length()] ;
        final String[] idlist = new String[potjAr.length()] ;
         connectlist = new String[potjAr.length()] ;

        for(int i=0; i< namelist.length ;i++)
        {
            namelist[i] = output(getJsonMessage(potURL), "deviceName",i);
            idlist[i] = output(getJsonMessage(potURL), "deviceId",i);
            connectlist[i] = output(getJsonMessage(potURL), "connected",i);
        }

        for(int j=0; j<namelist.length;j++){
            adapter.addItem(namelist[j],idlist[j],connectlist[j]);
        }


    }



}
