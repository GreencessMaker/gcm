package co.saru.greencessmaker;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * Created by saru on 2016. 11. 25..
 */



public class testtest extends Activity{
    private String deviceID = "1";
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:8081");
        } catch (URISyntaxException e) {}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.asdf);

        final String[] DeviceInfo = {"Now Loading","Now Loading","Now Loading","Now Loading","Now Loading","Now Loading"};

        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                mSocket.emit("phone-join", "youl");
                mSocket.emit("phone-socket", deviceID);
                // mSocket.disconnect();
            }

        }).on("DeviceInfo", new Emitter.Listener() {


            @Override
            public void call(Object... args) {

            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {

            }

        });
        mSocket.connect();

        mSocket.on("DeviceInfo", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                String deviceId = null;
                String deviceName = null;
                String temperature = null;
                String humidity = null;
                String light = null;
                String waterHeight = null;


                JSONObject jAr = (JSONObject) args[0];

                try {
                    deviceId = jAr.getString("deviceId");
                    deviceName = jAr.getString("deviceName");
                    temperature = jAr.getString("temperature");
                    humidity = jAr.getString("humidity");
                    light = jAr.getString("light");
                    waterHeight = jAr.getString("waterHeight");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Log.d("@@@@@@@@", strData.toString());
                DeviceInfo[0] = deviceId.toString();
                DeviceInfo[1] = deviceName.toString();
                DeviceInfo[2] = temperature.toString();
                DeviceInfo[3] = humidity.toString();
                DeviceInfo[4] = light.toString();
                DeviceInfo[5] = waterHeight.toString();


            }
        });


        Thread readyThread = new Thread(new Runnable() {
            int a = 0;
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            TextView TVdeviceID = (TextView) findViewById(R.id.deviceID);
                            TextView TVdeviceName = (TextView) findViewById(R.id.deviceName);
                            TextView TVtemperature = (TextView) findViewById(R.id.temperature);
                            TextView TVhumidity = (TextView) findViewById(R.id.humidity);
                            TextView TVlight = (TextView) findViewById(R.id.light);
                            TextView TVwaterHeight = (TextView) findViewById(R.id.waterHeight);

                            TVdeviceID.setText("ID : "+DeviceInfo[0]);
                            TVdeviceName.setText("Name : "+DeviceInfo[1]);
                            TVtemperature.setText("temperature : "+DeviceInfo[2]);
                            TVhumidity.setText("humidity : "+DeviceInfo[3]);
                            TVlight.setText("light : "+DeviceInfo[4]);
                            TVwaterHeight.setText("waterHeight : "+DeviceInfo[5]);



                        }
                    });
                }
            }
        });
        readyThread.start();

        Button watering = (Button)findViewById(R.id.watering);
        watering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSocket.emit("waterNow", deviceID);

                mSocket.on("waterNowSuccess", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Thread readyThread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "물주기에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                        readyThread.start();

                    }
                });

                mSocket.on("waterNowFail", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Thread readyThread = new Thread(new Runnable() {
                                                        @Override
                            public void run() {
                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "물주기에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                        });
                        readyThread.start();
                    }
                });

            }
        });



    }

    }


