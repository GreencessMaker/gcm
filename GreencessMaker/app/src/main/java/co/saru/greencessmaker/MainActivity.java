package co.saru.greencessmaker;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String deviceID = "0";
    private boolean flag = true;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:8081");
        } catch (URISyntaxException e) {}
    }

    private Thread readyThread;
    private String[] DeviceInfo;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        String type = intent.getStringExtra("type");
        deviceID = type;
        DeviceInfo = new String[]{"Now Loading", "Now Loading", "Now Loading", "Now Loading", "Now Loading", "Now Loading"};
        mSocket.connect();
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                mSocket.emit("phone-join", "youl");
            }
        }).on("waterNowFail", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Thread failThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "물주기에 실패하였습니다.", Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                });
                failThread.start();

            }
        }).on("waterNowSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Thread successThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "물주기에 성공하였습니다.", Toast.LENGTH_SHORT).show();


                            }
                        });
                    }
                });
                successThread.start();

            }
        }).on("DeviceInfo", new Emitter.Listener() {
            @Override
            public void call(Object... args) {


                String deviceId = null;
                String deviceName = null;
                String temperature = null;
                String humidity = null;
                String light = null;
                String waterHeight = null;


                JSONObject jAr = (JSONObject) args[0];

                try {
                    deviceId = jAr.getString("deviceId");
                    deviceName = jAr.getString("deviceName");
                    temperature = jAr.getString("temperature");
                    humidity = jAr.getString("humidity");
                    light = jAr.getString("light");
                    waterHeight = jAr.getString("waterHeight");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Log.d("@@@@@@@@", strData.toString());
                DeviceInfo[0] = deviceId.toString();
                DeviceInfo[1] = deviceName.toString();
                DeviceInfo[2] = temperature.toString();
                DeviceInfo[3] = humidity.toString();
                DeviceInfo[4] = light.toString();
                DeviceInfo[5] = waterHeight.toString();

            }
        });


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        change(2);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_information) {
            change(2);

            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            change(1);


        } else if (id == R.id.nav_setting) {
            change(3);

        } else if (id == R.id.nav_history) {
            change(4);

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change(int set){


        /*
        Intent intent = getIntent();
        String type = intent.getStringExtra("type");
        String URL="http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getDeviceInfoByDeviceId/";
        URL = URL+type;

        String jsonMessage = getJsonMessage(URL);
        JSONArray jAr = null;
        try {
            jAr = new JSONArray(jsonMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String deviceName = output(jsonMessage, "deviceName");
        String temperature = output(jsonMessage, "temperature");
        String humidity = output(jsonMessage, "humidity");
        String light = output(jsonMessage, "light");
        String waterHeight = output(jsonMessage, "waterHeight");



*/

        if(set == 1)
        {
            setContentView(R.layout.page1);
            ListView imageListView = (ListView)findViewById(R.id.potimage_ListView);
            showImageList(imageListView);


            /*
            final String imageURL="http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getImagesByDeviceId/1";

            JSONArray imagejAr = null;
            try {
                imagejAr = new JSONArray(getJsonMessage(imageURL));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final String[] list = new String[imagejAr.length()] ;
            for(int i=0; i< list.length ;i++)
            {
                list[i] = output2(getJsonMessage(imageURL), "imageId",i);
            }


            final ArrayAdapter<String> imageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list);
            final ListView imageListView = (ListView)findViewById(R.id.potimage_ListView);
            imageListView.setAdapter(imageAdapter);

*/


        }
        else if(set ==2 )
        {
            setContentView(R.layout.page2);
            mSocket.emit("phone-socket", deviceID);
            /*
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    mSocket.emit("phone-join", "youl");
                    mSocket.emit("phone-socket", deviceID);
                    Log.d("2@@@@@", "here???");

                }
            })*/
            readyThread = new Thread(new Runnable() {
                int a = 0;
                @Override
                public void run() {
                    while (flag) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                TextView TVdeviceID = (TextView) findViewById(R.id.deviceID);
                                TextView TVdeviceName = (TextView) findViewById(R.id.deviceName);
                                TextView TVtemperature = (TextView) findViewById(R.id.temperature);
                                TextView TVhumidity = (TextView) findViewById(R.id.humidity);
                                TextView TVlight = (TextView) findViewById(R.id.light);
                                TextView TVwaterHeight = (TextView) findViewById(R.id.waterHeight);

                                TVdeviceID.setText(DeviceInfo[0]);
                                TVdeviceName.setText(DeviceInfo[1]);
                                TVtemperature.setText(DeviceInfo[2]);
                                TVhumidity.setText(DeviceInfo[3]);
                                TVlight.setText(DeviceInfo[4]);
                                TVwaterHeight.setText(DeviceInfo[5]);
                            }
                        });
                    }
                }
            });
            readyThread.start();

            final Button watering = (Button)findViewById(R.id.watering);


            watering.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSocket.emit("waterNow", deviceID);

                }
            });



            ImageView IVpotImage = (ImageView)findViewById(R.id.potImage);
            try {
                String recentPotImageJsonMessage = getJsonMessage("http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getImagesByDeviceId/"+deviceID);
                String newestImageId=  output(recentPotImageJsonMessage,"imageId");
                String imageURLaddress = "http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/images/"+deviceID+"-"+newestImageId+".png";
                java.net.URL url = new URL(imageURLaddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                Bitmap bm = BitmapFactory.decodeStream(bis);
                bis.close();
                IVpotImage.setImageBitmap(bm);
            } catch (Exception e) {
            }







/*  소켓연결 쌤플.

            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    mSocket.emit("phone-join", "youl");
                    mSocket.emit("phone-socket", "1");
                   // mSocket.disconnect();
                }

            }).on("DeviceInfo", new Emitter.Listener() {


                @Override
                public void call(Object... args) {

                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                }

            });
            mSocket.connect();

            mSocket.on("DeviceInfo", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    String strData = null;

                    JSONObject jAr = (JSONObject) args[0];


                    try {
                        strData = jAr.getString("deviceName");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("@@@@@@@@", strData.toString());





                }
            });

*/

        }

        else if(set ==3 ) {
            // readyThread.interrupt();


            setContentView(R.layout.page3);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            final HttpClient client = new DefaultHttpClient();
            String postURL = "http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/modifyWateringInfo";
            final String getURL = "http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/selectWateringInfo/"+deviceID;
            final HttpPost post = new HttpPost(postURL);



            final ToggleButton TBmon = (ToggleButton)findViewById(R.id.mon);
            final ToggleButton TBtue = (ToggleButton)findViewById(R.id.tue);
            final ToggleButton TBweb = (ToggleButton)findViewById(R.id.wed);
            final ToggleButton TBthur = (ToggleButton)findViewById(R.id.thur);
            final ToggleButton TBfri = (ToggleButton)findViewById(R.id.fri);
            final ToggleButton TBsat = (ToggleButton)findViewById(R.id.sat);
            final ToggleButton TBsun = (ToggleButton)findViewById(R.id.sun);
            final TextView TVhour = (TextView)findViewById(R.id.hour);
            final TextView TVminute = (TextView)findViewById(R.id.minute);
            final TextView TVamount = (TextView)findViewById(R.id.amountText);
            final SeekBar seekbar = (SeekBar) findViewById(R.id.seekBar);



            TBmon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBmon.isChecked()){
                        TBmon.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.mon1)
                        );
                    }
                    else{
                        TBmon.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.mon2)
                        );
                    }
                }
            });
            TBtue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBtue.isChecked()){
                        TBtue.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.tue1)
                        );
                    }
                    else{
                        TBtue.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.tue2)
                        );
                    }
                }
            });
            TBweb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBweb.isChecked()){
                        TBweb.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.wed1)
                        );
                    }
                    else{
                        TBweb.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.wed2)
                        );
                    }
                }
            });
            TBthur.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBthur.isChecked()){
                        TBthur.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.thur1)
                        );
                    }
                    else{
                        TBthur.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.thur2)
                        );
                    }
                }
            });
            TBfri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBfri.isChecked()){
                        TBfri.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.fri1)
                        );
                    }
                    else{
                        TBfri.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.fri2)
                        );
                    }
                }
            });
            TBsat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TBsat.isChecked()){
                        TBsat.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.sat1)
                        );
                    }
                    else{
                        TBsat.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.sat2)
                        );
                    }
                }
            });
            TBsun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TBsun.isChecked()) {
                        TBsun.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.sun1)
                        );
                    } else {
                        TBsun.setBackgroundDrawable(
                                getResources().getDrawable(R.drawable.sun2)
                        );
                    }
                }
            });


            String getInfo = getJsonMessage(getURL);
            TVhour.setText(output(getInfo,"hour")+" 시");
            TVminute.setText(output(getInfo,"minute")+" 분");
            TVamount.setText(output(getInfo, "amount"));
            seekbar.setProgress(Integer.parseInt(output(getInfo, "amount")));
            if(output(getInfo,"mon").equals("1")){
                TBmon.setChecked(true);
                TBmon.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.mon1)
                );
            }
            if(output(getInfo,"tue").equals("1")){
                TBtue.setChecked(true);
                TBtue.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.tue1)
                );

            }
            if(output(getInfo,"wed").equals("1")){
                TBweb.setChecked(true);
                TBweb.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.wed1)
                );
            }
            if(output(getInfo,"thur").equals("1")) {
                TBthur.setChecked(true);
                TBthur.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.thur1)
                );
            }
            if(output(getInfo,"fri").equals("1")){
                TBfri.setChecked(true);
                TBfri.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.fri1)
                );
            }
            if(output(getInfo,"sat").equals("1")){
                TBsat.setChecked(true);
                TBsat.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.sat1)
                );
            }
            if(output(getInfo,"sun").equals("1")){
                TBsun.setChecked(true);
                TBsun.setBackgroundDrawable(
                        getResources().getDrawable(R.drawable.sun1)
                );
            }







            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
// 메소드 이름대로 사용자가 SeekBar를 터치했을때 실행됩니다
                    // TODO Auto-generated method stub
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
// 메소드 이름대로 사용자가 SeekBar를 손에서 땠을때 실행됩니다
                    // TODO Auto-generated method stub

                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
// 메소드 이름대로 사용자가 SeekBar를 움직일때 실행됩니다
// 주로 사용되는 메소드 입니다
                    // TODO Auto-generated method stub


                    seekbar.setProgress(progress);
                    TVamount.setText(String.valueOf(progress));

                    WindowManager.LayoutParams params = getWindow().getAttributes();
                    getWindow().setAttributes(params);

                }
            });



            Button sandAlarm = (Button)findViewById(R.id.sendOption);

            sandAlarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        String mon = "";
                        String tue = "";
                        String wed = "";
                        String thur = "";
                        String fri = "";
                        String sat = "";
                        String sun = "";
                        String amount = TVamount.getText().toString();
                        String hour = TVhour.getText().toString();
                        String minute = TVminute.getText().toString();

                        if (TBmon.isChecked() == true) {
                            mon = "1";
                        } else {
                            mon = "0";
                        }

                        if (TBtue.isChecked() == true) {
                            tue = "1";
                        } else {
                            tue = "0";
                        }

                        if (TBweb.isChecked() == true) {
                            wed = "1";
                        } else {
                            wed = "0";
                        }

                        if (TBthur.isChecked() == true) {
                            thur = "1";
                        } else {
                            thur = "0";
                        }

                        if (TBfri.isChecked() == true) {
                            fri = "1";
                        } else {
                            fri = "0";
                        }

                        if (TBsat.isChecked() == true) {
                            sat = "1";
                        } else {
                            sat = "0";
                        }

                        if (TBsun.isChecked() == true) {
                            sun = "1";
                        } else {
                            sun = "0";
                        }


                        List<NameValuePair> option = new ArrayList<NameValuePair>(2);
                        option.add(new BasicNameValuePair("deviceId", deviceID));
                        option.add(new BasicNameValuePair("mon", mon));
                        option.add(new BasicNameValuePair("tue", tue));
                        option.add(new BasicNameValuePair("wed", wed));
                        option.add(new BasicNameValuePair("thur", thur));
                        option.add(new BasicNameValuePair("fri", fri));
                        option.add(new BasicNameValuePair("sat", sat));
                        option.add(new BasicNameValuePair("sun", sun));
                        option.add(new BasicNameValuePair("amount", amount));
                        option.add(new BasicNameValuePair("hour", hour));
                        option.add(new BasicNameValuePair("minute", minute));

                        post.setEntity(new UrlEncodedFormEntity(option));

                        HttpResponse response = client.execute(post);
                        HttpEntity resEntity = response.getEntity();
                        if (resEntity != null) {
                            Log.i("RESPONSE@@@@", EntityUtils.toString(resEntity));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*
                    mSocket.connect();
                    mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            mSocket.emit("phone-join", "youl");
                            mSocket.emit("modifyWateringInfo", deviceID);

                            mSocket.disconnect();
                        }
                    });
*/
                    mSocket.emit("modifyWateringInfo", deviceID);
                    Toast.makeText(MainActivity.this, "전송되었습니다.", Toast.LENGTH_SHORT).show();

                }
            });

            Button setTime = (Button)findViewById(R.id.settime);
            setTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new TimePickerDialog(MainActivity.this, timeSetListener, 12, 0, false).show();
                }
            });



        }




        else if(set ==4 ) {
            setContentView(R.layout.page4);
            String history = getJsonMessage("http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getDeviceHistories/"+deviceID);
            int[] temperature = {0,0,0,0,0,0,0,0,0,0,0};
            int[] humidity = {0,0,0,0,0,0,0,0,0,0,0};

            String flag;

            for(int i =0; i<11 ; i++){
                flag = output2(history, "temperature", i);
                temperature[i] = Integer.parseInt(flag);
                flag = output2(history, "humidity", i);
                humidity[i] = Integer.parseInt(flag);
            }

            //그래프 생성
            final GraphicalView chart = ChartFactory.getLineChartView(this, getDataset(temperature,humidity), getRenderer());
            //레이아웃에 추가
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.chart);
            layout.addView(chart);
        }




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public String getJsonMessage(String URL) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String send="plz";

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(URL);

            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();

            if (resEntityGet != null) {
                //do something with the response
                // Log.d("GET RESPONSE", EntityUtils.toString(resEntityGet));
                send = EntityUtils.toString(resEntityGet);
                //String send = URLEncoder.encode("한글","utf-8");  // 원래는 utf-8 처리를 하는게 맞음.
                return send;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return send;
    }

    public String output(String input, String fistValue){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            //  JSONArray jAr = new JSONArray("["+input+"]");
            JSONArray jAr = new JSONArray(input);
            // 개별 객체를 하나씩 추출
            JSONObject obj1 = jAr.getJSONObject(0);
            // 객체에서 데이터를 추출
            strData = obj1.getString(fistValue);

        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

    public String output2(String input, String fistValue ,int i){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            //  JSONArray jAr = new JSONArray("["+input+"]");
            JSONArray jAr = new JSONArray(input);
            // 개별 객체를 하나씩 추출
            JSONObject obj1 = jAr.getJSONObject(i);
            // 객체에서 데이터를 추출
            strData += obj1.getString(fistValue);

        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

    // 뷰홀더 생성
    private static class ViewHolder{
        ImageView image;

    }


    public void showImageList(ListView lv){
        ListView listview ;
        final imageViewAdapter adapter;


        // Adapter 생성
        adapter = new imageViewAdapter() ;
        // 리스트뷰 참조 및 Adapter달기
        listview = lv;
        listview.setAdapter(adapter);

        final String imageURL="http://ec2-52-78-120-48.ap-northeast-2.compute.amazonaws.com:3000/database/getImagesByDeviceId/"+deviceID;
        JSONArray imagejAr = null;
        try {
            imagejAr = new JSONArray(getJsonMessage(imageURL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] imagelist = new String[imagejAr.length()] ;
        final String[] datelist = new String[imagejAr.length()] ;

        for(int i=0; i< imagelist.length ;i++)
        {
            imagelist[i] = output2(getJsonMessage(imageURL), "imageId",i);
            datelist[i] = output2(getJsonMessage(imageURL), "date",i);
        }

        for(int j=0; j<imagelist.length;j++){
            adapter.addItem(imagelist[j],datelist[j]);
        }


    }

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // TODO Auto-generated method stub
            final TextView TVminute = (TextView)findViewById(R.id.minute);
            final TextView TVhour = (TextView)findViewById(R.id.hour);
            TVminute.setText(String.valueOf(minute)+ "분");
            TVhour.setText(String.valueOf(hourOfDay)+ "시");
            Toast.makeText(MainActivity.this, "시간이 설정되었습니다.", Toast.LENGTH_SHORT).show();
        }
    };




    //그래프 설정 모음
    // http://www.programkr.com/blog/MQDN0ADMwYT3.html ( 그래프 설정 속성 한글로 써져있는 사이트 )
    private void setChartSettings(XYMultipleSeriesRenderer renderer) {
        //타이틀, x,y축 글자
        renderer.setChartTitle("현황보기");
        renderer.setXTitle("시간(전)");
        renderer.setYTitle("값");

        renderer.setRange(new double[] {0,6,-70,40});


        //background
        renderer.setApplyBackgroundColor(true);      //변경 가능여부
        renderer.setBackgroundColor(Color.WHITE);    //그래프 부분 색
        renderer.setMarginsColor(Color.WHITE);       //그래프 바깥 부분 색(margin)

        //글자크기
        renderer.setAxisTitleTextSize(40);          //x,y축 title
        renderer.setChartTitleTextSize(40);         //상단 title
        renderer.setLabelsTextSize(30);             //x,y축 수치
        renderer.setLegendTextSize(30);             //Series 구별 글씨 크기
        renderer.setPointSize(8f);
        renderer.setMargins(new int[] { 50, 50, 70, 50 }); //상 좌 하 우 ( '하' 의 경우 setFitLegend(true)일 때에만 가능 )

        //색
        renderer.setAxesColor(Color.RED);       //x,y축 선 색
        renderer.setLabelsColor(Color.GREEN);    //x,y축 글자색

        //x,y축 표시 간격 ( 각 축의 범위에 따라 나눌 수 있는 최소치가 제한 됨 )
        renderer.setXLabels(5);
        renderer.setYLabels(5);

        //x축 최대 최소(화면에 보여질)
        renderer.setXAxisMin(0);
        renderer.setXAxisMax(10);
        //y축 최대 최소(화면에 보여질)
        renderer.setYAxisMin(20);
        renderer.setYAxisMax(35);

        //클릭 가능 여부
        renderer.setClickEnabled(true);
        //줌 기능 가능 여부
        renderer.setZoomEnabled(false,false);
        //X,Y축 스크롤
        renderer.setPanEnabled(true, false);                // 가능 여부
        renderer.setPanLimits(new double[]{-2,24,20,40} );   // 가능한 범위

        //지정된 크기에 맞게 그래프를 키움
        renderer.setFitLegend(true);
        //간격에 격자 보이기
        renderer.setShowGrid(true);

    }

    //선 그리기
    private XYMultipleSeriesRenderer getRenderer() {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();

        //---그려지는 점과 선 설정----
        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setColor(Color.BLACK);            //색
        r.setPointStyle(PointStyle.DIAMOND);//점의 모양
        r.setFillPoints(false);             //점 체우기 여부
        renderer.addSeriesRenderer(r);
        //----------------------------

        /*
        * 다른 그래프를 추가하고 싶으면
        * XYSeriesRenderer 추가로 생성한 후
        *  renderer.addSeriesRenderer(r) 해준다 (Data도 있어야함)
        *
        */
        XYSeriesRenderer a = new XYSeriesRenderer();
        a.setColor(Color.RED);            //색
        a.setPointStyle(PointStyle.DIAMOND);//점의 모양
        a.setFillPoints(false);             //점 체우기 여부
        renderer.addSeriesRenderer(a);


        setChartSettings(renderer);
        return renderer;
    }

    //데이터들
    private XYMultipleSeriesDataset getDataset( int[] temperature, int[] humidity ) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();


        XYSeries series = new XYSeries("온도");
        for (int i = 0; i < temperature.length; i++ ) {
            series.add(i, temperature[i] );
        }
        XYSeries series2 = new XYSeries("습도");
        for (int i = 0; i < humidity.length; i++ ) {
            series2.add(i, humidity[i]+10 );
        }

        /*
        *
        * 다른 그래프를 추가하고 싶으면
        * XYSeries를 추가로 생성한 후
        * dataset.addSeries(series) 해준다 (renderer도 있어야함)
        *
        */


        dataset.addSeries(series);
        dataset.addSeries(series2);

        return dataset;
    }







}